$(function () {
    $('select').formSelect();
    $('.tooltipped').tooltip();
    $('textarea').characterCounter();

    let dvPage = '#page';
    let inputSenha = '#senha';
    let confSenha = '#confSenha';
    let dvRemuneracao = '#remuneracao';
    let inputValidarSenha = '#validPassword';

    $('#cep').mask('00000-000');
    $('#anoIngresso').mask('0000');
    $(dvRemuneracao).mask('999.990,00', {reverse: true});

    if ($(dvPage).val() === 'listarVagas') {
        $('.modal').modal({
            dismissible: false,
            onOpenEnd: function () {
                $('#btnExcluirVaga').attr('href', $('#excluirVaga').val());
            },
            onCloseEnd: function () {
                $('#excluirVaga').val('');
            }
        });

        $('.modal-trigger').click(function () {
            $('#excluirVaga').val($(this).data('content'));
        });
    }

    $(dvRemuneracao).focusout(function () {
        if ($(this).val().replace(',', '.') < 600) {
            $(this).addClass('invalid');
            $(this).parent().find('span.helper-text').attr('data-error', 'Favor inserir um valor igual ou maior a 600 reais.');
        } else {
            $(this).removeClass('invalid');
            $(this).parent().find('span.helper-text').attr('data-error', 'Campo de preenchimento obrigatório');
        }
    });

    $('input, select, textarea').each(function () {
        if ($(this).attr('required')) {

            if ($(this).attr('id')) {
                $(this).parents('.input-field').children('label').prepend(' <span class="text-danger">*</span> ');
            } else if ($('input[type="radio"]') && !$(this).parents('.input-field').children('span').find('.text-danger')[0]) {
                $(this).parents('.input-field').children('span').prepend(' <span class="text-danger">*</span> ');
            }
        }
    });

    if ($(dvPage).val() === 'cadastroUsuario') {
        let dvEstagiario = '#dvEstagiario';
        let dvEmpregador = '#dvEmpregador'

        document.querySelector('input[type=email]').oninvalid = function () {
            this.setCustomValidity("");

            if (!this.validity.valid) {
                this.setCustomValidity("Informe um e-mail válido.");
            }
        };

        $("input[name='tpAcesso']").on('change', function () {
            if ($(this).val() === '1') {
                $(dvEmpregador).addClass('hide');
                $(dvEstagiario).removeClass('hide');

                $(dvEstagiario).children().each(function () {
                    $(this).children().each(function () {
                        $(this).find('input, textarea').val('').prop("disabled", false);
                    });
                });

                $(dvEmpregador).children().each(function () {
                    $(this).children().each(function () {
                        $(this).find('input, textarea').val('').prop("disabled", true);
                    });
                });
            } else if ($(this).val() === '2') {
                $(dvEstagiario).addClass('hide');
                $(dvEmpregador).removeClass('hide');

                $(dvEmpregador).children().each(function () {
                    $(this).children().each(function () {
                        $(this).find('input, textarea').val('').prop("disabled", false);
                    });
                });

                $(dvEstagiario).children().each(function () {
                    $(this).children().each(function () {
                        $(this).find('input, textarea').val('').prop("disabled", true);
                    });
                });
            }
        });

        $(inputSenha).keyup(function () {
            let senhaInformada = $(this).val();
            let regexLM = /^(?=(?:.*?[A-Z]){1})/; // Mínimo 1 letra maiúscula
            let regexCN = /^(?=(?:.*?[0-9]){1})/; // Mínimo 1 número
            let regexCE = /^(?=(?:.*?[!@#$%*()_+^&}{:;?.]){1})(?!.*\s)[0-9a-zA-Z!@#$%;*(){}_+^&]*$/; // Mínimo 1 especial

            if (senhaInformada.length !== 5) {
                $('#regraTam').addClass('text-danger');
                $(this).parent().find('span.helper-text').attr('data-error', 'A senha informada não atende aos critérios de aceitação.');
            } else if (senhaInformada.length === 5) {
                $('#regraTam').removeClass('text-danger');
                $(this).removeClass('invalid');
            }

            if (!regexLM.exec(senhaInformada)) {
                $('#regraLM').addClass('text-danger');
                $(this).parent().find('span.helper-text').attr('data-error', 'A senha informada não atende aos critérios de aceitação.');
            } else if (regexLM.exec(senhaInformada)) {
                $('#regraLM').removeClass('text-danger');
                $(this).removeClass('invalid');
            }

            if (!regexCN.exec(senhaInformada)) {
                $('#regraCN').addClass('text-danger');
                $(this).parent().find('span.helper-text').attr('data-error', 'A senha informada não atende aos critérios de aceitação.');
            } else if (regexCN.exec(senhaInformada)) {
                $('#regraCN').removeClass('text-danger');
                $(this).removeClass('invalid');
            }

            if (!regexCE.exec(senhaInformada)) {
                $('#regraCE').addClass('text-danger');
                $(this).parent().find('span.helper-text').attr('data-error', 'A senha informada não atende aos critérios de aceitação.');
            } else if (regexCE.exec(senhaInformada)) {
                $('#regraCE').removeClass('text-danger');
                $(this).removeClass('invalid');
            }
        });

        $(confSenha).keyup(function () {
            if ($(this).val() !== undefined && $(this).val() !== $(inputSenha).val()) {
                $(this).addClass('invalid');
                $(this).parent().find('span.helper-text').attr('data-error', 'As senhas não coincidem.');
            } else {
                $(this).removeClass('invalid');
                $(this).parent().find('span.helper-text').attr('data-error', 'Campo de preenchimento obrigatório');
            }
        });

        $(confSenha).focusout(function () {
            if ($(this).val() !== undefined && $(this).val() !== $(inputSenha).val()) {
                $(this).addClass('invalid');
                $(this).parent().find('span.helper-text').attr('data-error', 'As senhas não coincidem.');
            } else {
                $(this).removeClass('invalid');
                $(this).parent().find('span.helper-text').attr('data-error', 'Campo de preenchimento obrigatório');
            }
        });

        $(senha).focusout(function () {
            $('p.my-0').each(function () {
                if ($(this).hasClass('text-danger')) {
                    $(inputValidarSenha).val(0);
                    $(inputSenha).addClass('invalid');
                    $(inputSenha).parent().find('span.helper-text').attr('data-error', 'A senha informada não atende aos critérios de aceitação.');
                }
            });

            if ($(this).val() !== undefined && $(this).val() !== $(confSenha).val()) {
                $(confSenha).addClass('invalid');
                $(confSenha).parent().find('span.helper-text').attr('data-error', 'As senhas não coincidem.');
            } else {
                $(this).removeClass('invalid');
                $(confSenha).removeClass('invalid');
                $(this).parent().find('span.helper-text').attr('data-error', 'Campo de preenchimento obrigatório');
                $(confSenha).parent().find('span.helper-text').attr('data-error', 'Campo de preenchimento obrigatório');
            }
        });
    }
});