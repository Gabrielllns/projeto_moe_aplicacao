<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col s8 offset-s2">
        <nav class="mt-5 mb-0 cyan darken-1">
            <div class="nav-wrapper">
                <form id="formPesquisa" method="POST" action="<?= route_to('pesquisarEmpresaPorNome') ?>">
                    <div class="input-field">
                        <input id="search" type="search" placeholder="Buscar Empresas" name="nomeEmpresa" maxlength="80"
                               class="validate" value="<?= old('nomeEmpresa') ?>" required>
                    </div>
                </form>
            </div>
        </nav>
    </div>
</div>

<div class="section">
    <div class="row">
        <?= $this->include('partials/alertas') ?>

        <div class="text-center mb-5">
            <h5 class="text-muted">Vagas adicionadas recentemente</h5>
        </div>

        <?php if (!empty($ultimasVagas)) : ?>
            <?php foreach ($ultimasVagas as $ultimaVaga) : ?>
                <div class="col s4">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="card-title mb-3">
                                    <?= $ultimaVaga->nomeEmpresa ?> - <?= $ultimaVaga->nomeVaga ?>
                                </span>
                                <p class="text-muted"><?= $ultimaVaga->totalVagas ?> vaga(s)</p>
                            </div>
                            <?php if (empty(session(SESSION_USER)) || intval(session(SESSION_USER)->perfil) == PERFIL_ESTAGIARIO) : ?>
                                <div class="card-action d-flex justify-content-around">
                                    <?php if ($ultimaVaga->hasCandidatura) : ?>
                                        <a href="<?= route_to('removerInteresse', $ultimaVaga->idEmpresa) ?>"
                                           class="text-default mx-0"> Deixar de Seguir
                                        </a>
                                    <?php else: ?>
                                        <a href="<?= route_to('adicionarInteresse', $ultimaVaga->idEmpresa) ?>"
                                           class="text-default mx-0"> Seguir
                                        </a>
                                    <?php endif; ?>

                                    <div class="vertical-line"></div>

                                    <a href="<?= route_to('listarVagasPorEmpresa', $ultimaVaga->idEmpresa) ?>"
                                       class="text-default mx-0"> Vagas
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if (!empty($vagasEmpresa)) : ?>
            <?php foreach ($vagasEmpresa as $vagaEmpresa) : ?>
                <div class="col s4">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="card-title mb-3">
                                    <?= $vagaEmpresa->nome ?> - <?= $vagaEmpresa->descricao ?>
                                </span>
                                <p class="text-muted"><?= $vagaEmpresa->totalVagas ?> vaga(s)</p>
                            </div>
                            <?php if (empty(session(SESSION_USER)) || intval(session(SESSION_USER)->perfil) == PERFIL_ESTAGIARIO) : ?>
                                <div class="card-action d-flex justify-content-around">
                                    <?php if ($vagaEmpresa->hasCandidatura) : ?>
                                        <a href="<?= route_to('removerInteresse', $vagaEmpresa->id) ?>"
                                           class="text-default mx-0"> Deixar de Seguir
                                        </a>
                                    <?php else: ?>
                                        <a href="<?= route_to('adicionarInteresse', $vagaEmpresa->id) ?>"
                                           class="text-default mx-0"> Seguir
                                        </a>
                                    <?php endif; ?>

                                    <div class="vertical-line"></div>

                                    <a href="<?= route_to('listarVagasPorEmpresa', $vagaEmpresa->id) ?>"
                                       class="text-default mx-0"> Vagas
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php elseif (empty($ultimasVagas)): ?>
            <div class="alert alert-warning">Empresa não encontrada.</div>
        <?php endif; ?>

    </div>
</div>
<?= $this->endSection() ?>
