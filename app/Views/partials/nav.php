<nav class="white nav-extended px-5">
    <div class="d-flex justify-content-between align-items-center py-3 px-5">
        <div class="col m2">
            <a href="<?= route_to('homePage') ?>" class="brand-logo">
                <img class="responsive-img nav-img-height" src="<?= base_url('img/logo.png') ?>">
            </a>
        </div>

        <div class="col m10">
            <div class="nav-wrapper">
                <?php if (session(SESSION_USER)): ?>
                    <a href="#" class="d-flex text-default right pl-3 profile">
                        <?= (session(SESSION_USER)->perfil == PERFIL_EMPREGADOR) ? 'Empregador' : 'Estagiário' ?>
                        | <?= session(SESSION_USER)->nome ?><i class="pl-2 material-icons icon pr-3">account_circle</i>
                    </a>
                <?php endif; ?>
            </div>

            <div class="nav-wrapper">
                <ul class="right hide-on-med-and-down">
                    <?php if (session(SESSION_USER)): ?>
                        <li>
                            <a href="<?= route_to('homePage') ?>" class="text-default">
                                Home
                            </a>
                        </li>

                        <?php if (session(SESSION_USER)->perfil == PERFIL_EMPREGADOR) : ?>
                            <li>
                                <a href="<?= route_to('listarVagasPage') ?>" class="text-default">
                                    Vagas
                                </a>
                            </li>
                        <?php endif; ?>

                        <li>
                            <a href="<?= route_to('logoutPage') ?>" class="text-default">
                                Logout
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (!session(SESSION_USER)): ?>
                        <?php if (!isset(explode('/', current_url())[4])): ?>
                            <li>
                                <a href="<?= route_to('addUsuarioPage') ?>" class="text-default">
                                    Criar Conta
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="<?= route_to('homePage') ?>" class="text-default">
                                    Home
                                </a>
                            </li>
                        <?php endif; ?>
                        <li>
                            <a href="<?= route_to('loginPage') ?>" class="text-default">
                                Login
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</nav>