<?php if (session()->get('success')): ?>
    <div class="alert alert-success">
        <?= session()->get('success') ?>
    </div>
<?php endif; ?>

<?php if (session()->get('error')): ?>
    <div class="alert alert-error">
        <?= session()->get('error') ?>
    </div>
<?php endif; ?>

<?php if (session()->get('warning')): ?>
    <div class="alert alert-warning">
        <?= session()->get('warning') ?>
    </div>
<?php endif; ?>
