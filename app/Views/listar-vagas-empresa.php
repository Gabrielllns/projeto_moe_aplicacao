<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <input type="hidden" value="listarVagasEmpresa" id="page">

    <div class="section">
        <div class="row">
            <div class="d-flex justify-content-between align-items-center mb-4">
                <h5 class="text-default">
                    Listar Vagas - <?= (!empty($data['empresa'])) ? $data['empresa']->nome : '' ?>
                </h5>
            </div>

            <div class="row">
                <?= $this->include('partials/alertas') ?>

                <table class="highlight responsive-table centered">
                    <thead class="grey lighten-2">
                    <tr>
                        <th>Remuneração</th>
                        <th>Semestre do Curso</th>
                        <th>Qtde Horas</th>
                        <th>Descrição</th>
                        <th>Candidatura</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($data['vagasEmpresa'])): ?>
                        <?php foreach ($data['vagasEmpresa'] as $vaga): ?>
                            <tr>
                                <td>R$ <?= $vaga->remuneracao ?></td>
                                <td><?= $vaga->semestreCurso ?></td>
                                <td><?= $vaga->qtdeHoras ?></td>
                                <td><?= $vaga->descricao ?></td>
                                <td><?= ((!$vaga->hasCandidatura) ? 'Não interessado' : 'Interessado') ?></td>
                                <td>
                                    <a href="<?= route_to('visualizarVagaPage', $vaga->id) ?>">
                                        <i class="material-icons text-info mx-1 icon" title="Visualizar Vaga">search</i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="6">Não há vagas cadastradas.</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

                <?= $data['pager']->links() ?>
            </div>

            <div class="row text-center">
                <a href="<?= route_to('homePage') ?>" class="btn waves-effect grey lighten-1 mr-3"
                   type="button"> Voltar
                </a>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>