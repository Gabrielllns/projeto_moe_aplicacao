<?= $this->extend('templates/default') ?>

<?= $this->section('content') ?>

<div>
    <?= session('message') ?>
</div>
<div class="section">
    <div class="row">
        <div class="col s12">

            <div class="card horizontal px-5 py-5">
                <div class="card-image d-flex align-items-center">
                    <img class="responsive-img format-img-login" src="<?= base_url('img/logo.png') ?>">
                </div>

                <div class="vertical-line mx-5"></div>

                <div class="card-stacked">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h5 class="text-default">Pesquisa de Satisfação</h5>

                        <span class="text-danger">* Campos obrigatórios</span>
                    </div>

                    <div class="row">
                        <?= $this->include('partials/alertas') ?>

                        <form id="formPesquisa" method="POST" action="<?= route_to('confirmarUsuario') ?>">
                            <input type="hidden" name="token" value="<?= session()->get('token') ?>">
                            <div class="row">
                                <div class="input-field col s12 my-0">
                                    <span>1. Estou satisfeito quanto a facilidade para completar a tarefa:</span>

                                    <div class="mt-3 d-flex justify-content-around">
                                        <label>
                                            <input name="pergunta1" value="1" type="radio" required>
                                            <span>1</span>
                                        </label>

                                        <label>
                                            <input name="pergunta1" value="2" type="radio" required>
                                            <span>2</span>
                                        </label>

                                        <label>
                                            <input name="pergunta1" value="3" type="radio" required>
                                            <span>3</span>
                                        </label>

                                        <label>
                                            <input name="pergunta1" value="4" type="radio" required>
                                            <span>4</span>
                                        </label>

                                        <label>
                                            <input name="pergunta1" value="5" type="radio" required>
                                            <span>5</span>
                                        </label>
                                    </div>

                                    <div class="mt-2 d-flex justify-content-between">
                                        <small class="text-center">Totalmente<br>Insatisfeito</small>
                                        <small class="text-center">Totalmente<br>Satisfeito</small>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="input-field col s12 my-0">
                                    <span>2. Estou satisfeito quanto ao tempo para realizar a tarefa:</span>

                                    <div class="mt-3 d-flex justify-content-around">
                                        <label>
                                            <input name="pergunta2" value="1" type="radio" required>
                                            <span>1</span>
                                        </label>

                                        <label>
                                            <input name="pergunta2" value="2" type="radio" required>
                                            <span>2</span>
                                        </label>

                                        <label>
                                            <input name="pergunta2" value="3" type="radio" required>
                                            <span>3</span>
                                        </label>

                                        <label>
                                            <input name="pergunta2" value="4" type="radio" required>
                                            <span>4</span>
                                        </label>

                                        <label>
                                            <input name="pergunta2" value="5" type="radio" required>
                                            <span>5</span>
                                        </label>
                                    </div>

                                    <div class="mt-2 d-flex justify-content-between">
                                        <small class="text-center">Totalmente<br>Insatisfeito</small>
                                        <small class="text-center">Totalmente<br>Satisfeito</small>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="input-field col s12 my-0">
                                    <span>3. Estou satisfeito com o nível de suporte e informações enquanto completava a tarefa:</span>

                                    <div class="mt-3 d-flex justify-content-around">
                                        <label>
                                            <input name="pergunta3" value="1" type="radio" required>
                                            <span>1</span>
                                        </label>

                                        <label>
                                            <input name="pergunta3" value="2" type="radio" required>
                                            <span>2</span>
                                        </label>

                                        <label>
                                            <input name="pergunta3" value="3" type="radio" required>
                                            <span>3</span>
                                        </label>

                                        <label>
                                            <input name="pergunta3" value="4" type="radio" required>
                                            <span>4</span>
                                        </label>

                                        <label>
                                            <input name="pergunta3" value="5" type="radio" required>
                                            <span>5</span>
                                        </label>
                                    </div>

                                    <div class="mt-2 d-flex justify-content-between">
                                        <small class="text-center">Totalmente<br>Insatisfeito</small>
                                        <small class="text-center">Totalmente<br>Satisfeito</small>
                                    </div>
                                </div>
                            </div>

                            <div class="row center-align my-0">
                                <button class="btn waves-effect waves-light mr-3" type="submit">
                                    Concluir
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?= $this->endSection() ?>
