<!DOCTYPE html>
<html lang="pt">

<?= $this->include('partials/head') ?>

<body>
<?= $this->renderSection('nav') ?>

<div class="container">
    <?= $this->renderSection('content') ?>
</div>

<?= $this->include('partials/footer') ?>

<?= $this->include('partials/scripts') ?>
</body>

</html>
