<?= $this->extend('templates/default') ?>

<?= $this->section('content') ?>

<div>
    <?= session('message') ?>
</div>
<div class="section full-height">
    <div class="row">
        <div class="col s12">

            <div class="card horizontal px-5 py-5">
                <div class="card-image d-flex align-items-center">
                    <a href="<?= route_to('homePage') ?>">
                        <img class="responsive-img format-img-login" src="<?= base_url('img/logo.png') ?>">
                    </a>
                </div>

                <div class="vertical-line mx-5"></div>

                <div class="card-stacked">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-default">Login</h5>

                        <span class="text-danger">* Campos obrigatórios</span>
                    </div>

                    <div class="row">
                        <?= $this->include('partials/alertas') ?>

                        <form id="formLogin" method="POST" action="<?= route_to('realizarLogin') ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="email" name="email" maxlength="80" class="validate"
                                           value="<?= old('email') ?>" required>
                                    <label for="email">E-mail</label>
                                    <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                          data-success=""></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="senha" type="password" name="senha" maxlength="5" class="validate"
                                           value="<?= old('senha') ?>" required>
                                    <label for="senha">Senha</label>
                                    <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                          data-success=""></span>
                                </div>

                                <p class="ml-3">
                                    Não tem uma conta?
                                    <a href="<?= route_to('addUsuarioPage') ?>" class="text-default">
                                        Criar Conta
                                    </a>
                                </p>
                            </div>

                            <div class="row right-align">
                                <a href="<?= route_to('homePage') ?>" class="btn waves-effect grey lighten-1 mr-3"
                                   type="button"> Voltar
                                </a>

                                <button class="btn waves-effect waves-light mr-3" type="submit">
                                    Entrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?= $this->endSection() ?>
