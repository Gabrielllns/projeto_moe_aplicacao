<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="section">
    <div class="row">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h5 class="text-default">Visualizar Vaga</h5>
        </div>

        <div class="row">
            <?= $this->include('partials/alertas') ?>

            <?php if (!empty($vaga)) : ?>
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="<?= $vaga->semestreCurso ?>" disabled>
                        <label>Semestre do Curso</label>
                    </div>

                    <div class="input-field col s4">
                        <input type="text" value="<?= 'R$ ' . $vaga->remuneracao ?>" disabled>
                        <label>Remuneração</label>
                    </div>

                    <div class="input-field col s4">
                        <input type="text" value="<?= $vaga->qtdeHoras ?>" disabled>
                        <label>Qtde de Horas</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->descricao ?></textarea>
                        <label>Descrição Resumida da Vaga</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->atividades ?></textarea>
                        <label>Atividades</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->habilidades ?></textarea>
                        <label>Habilidades</label>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
