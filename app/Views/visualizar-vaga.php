<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="section">
    <div class="row">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <?php if (session(SESSION_USER)->perfil == PERFIL_EMPREGADOR) : ?>
                <h5 class="text-default">Visualizar Vaga</h5>
            <?php else: ?>
                <h5 class="text-default">Detalhes da Vaga</h5>
            <?php endif; ?>
        </div>

        <div class="row">
            <?php if (!empty($vaga)) : ?>
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="<?= $vaga->semestreCurso ?>" disabled>
                        <label>Semestre do Curso</label>
                    </div>

                    <div class="input-field col s4">
                        <input type="text" value="<?= 'R$ ' . $vaga->remuneracao ?>" disabled>
                        <label>Remuneração</label>
                    </div>

                    <div class="input-field col s4">
                        <input type="text" value="<?= $vaga->qtdeHoras ?>" disabled>
                        <label>Qtde de Horas</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->descricao ?></textarea>
                        <label>Descrição Resumida da Vaga</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->atividades ?></textarea>
                        <label>Atividades</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" disabled><?= $vaga->habilidades ?></textarea>
                        <label>Habilidades</label>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row text-center">
                <?php if (session(SESSION_USER)->perfil == PERFIL_EMPREGADOR) : ?>
                    <a href="<?= route_to('listarVagasPage') ?>" class="btn waves-effect grey lighten-1 mr-3"
                       type="button"> Voltar
                    </a>
                <?php else: ?>
                    <a href="<?= route_to('listarVagasPorEmpresa', $vaga->fk_empresa) ?>"
                       class="btn waves-effect grey lighten-1 mr-3"
                       type="button"> Voltar
                    </a>

                    <?php if (!$hasCandidatura) : ?>
                        <a href="<?= route_to('adicionarCandidatura', $vaga->id) ?>"
                           class="btn waves-effect mr-3"
                           type="button"> Candidatar-se
                        </a>
                    <?php else: ?>
                        <a href="<?= route_to('removerCandidatura', $vaga->id) ?>"
                           class="btn waves-effect red darken-1 mr-3"
                           type="button"> Remover Candidatura
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
