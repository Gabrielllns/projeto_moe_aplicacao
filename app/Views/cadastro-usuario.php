<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<input type="hidden" value="cadastroUsuario" id="page">

<div class="section">
    <div class="row">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h5 class="text-default">Criar Conta</h5>

            <span class="text-danger">* Campos obrigatórios</span>
        </div>

        <div class="row">
            <?= $this->include('partials/alertas') ?>

            <form id="formCadastroUsuario" method="POST" action="<?= route_to('cadastrarUsuario') ?>"
                  enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="email" type="email" name="email" maxlength="80" class="validate"
                               value="<?= old('email') ?>" required>
                        <label for="email">E-mail</label>
                        <span class="helper-text" data-error="Favor informar um e-mail válido" data-success=""></span>
                    </div>

                    <div class="input-field col s6">
                        <input id="senha" type="password" name="senha" maxlength="5" class="validate"
                               value="<?= old('senha') ?>" required>
                        <label for="senha">Senha</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input id="confSenha" type="password" name="confSenha" maxlength="5" class="validate"
                               value="<?= old('confSenha') ?>" required>
                        <label for="confSenha">Confirmação de senha</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>

                    <div class="col s6">
                        <div class="d-flex justify-content-between font-12" id="validadores">
                            <div>
                                <p class="my-0" id="regraLM">Pelo menos uma 1 letra maiúscula</p>
                                <p class="my-0" id="regraCN">Pelo menos um caracter numérico</p>
                            </div>
                            <div>
                                <p class="my-0" id="regraCE">Pelo menos um caracter especial</p>
                                <p class="my-0" id="regraTam">Menor que 5 caracteres</p>
                            </div>

                            <input type="hidden" value="0" id="validPassword">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <span>Perfil:</span>

                        <div class="mt-4">
                            <label class="mr-3">
                                <input name="tpAcesso" value="2" type="radio" required>
                                <span>Empregador</span>
                            </label>

                            <label>
                                <input name="tpAcesso" value="1" type="radio" required>
                                <span>Estagiário</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div id="dvEmpregador" class="hide">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="nomeEmpresa" type="text" name="nomeEmpresa" maxlength="100" class="validate"
                                   value="<?= old('nomeEmpresa') ?>" required>
                            <label for="nomeEmpresa">Nome da Empresa</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>

                        <div class="input-field col s6">
                            <input id="nomeResponsavel" type="text" name="nomeResponsavel" maxlength="100"
                                   class="validate" value="<?= old('nomeResponsavel') ?>" required>
                            <label for="nomeResponsavel">Nome do Responsável</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input id="cep" type="text" name="cep" maxlength="9" minlength="9" class="validate"
                                   value="<?= old('cep') ?>" required>
                            <label for="cep">CEP</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>

                        <div class="input-field col s6">
                            <input id="logradouro" type="text" name="logradouro" maxlength="100" class="validate"
                                   value="<?= old('logradouro') ?>" required>
                            <label for="logradouro">Logradouro</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input id="bairro" type="text" name="bairro" maxlength="100" class="validate"
                                   value="<?= old('bairro') ?>" required>
                            <label for="bairro">Bairro</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>

                        <div class="input-field col s6">
                            <input id="complemento" type="text" name="complemento" maxlength="200"
                                   value="<?= old('complemento') ?>" class="validate"
                                   required>
                            <label for="complemento">Complemento</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="dsEmpresa" class="materialize-textarea validate" name="dsEmpresa"
                                      data-length="1000" required><?= old('dsEmpresa') ?></textarea>
                            <label for="dsEmpresa">Descrição da Empresa</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>
                    </div>
                </div>

                <div id="dvEstagiario" class="hide">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="nomeCompleto" type="text" name="nomeCompleto" maxlength="100" class="validate"
                                   value="<?= old('nomeCompleto') ?>" required>
                            <label for="nomeCompleto">Nome Completo</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>

                        <div class="input-field col s6">
                            <input id="curso" type="text" name="curso" maxlength="100" class="validate"
                                   value="<?= old('curso') ?>" required>
                            <label for="curso">Curso</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input id="anoIngresso" type="text" name="anoIngresso" maxlength="4" minlength="4"
                                   class="validate" value="<?= old('anoIngresso') ?>" required>
                            <label for="anoIngresso">Ano de Ingresso</label>
                            <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                                  data-success=""></span>
                        </div>

                        <div class="file-field input-field col s6">
                            <div class="btn">
                                <span>Arquivo </span>
                                <span class="text-danger">*</span>
                                <input type="file" class="validate" name="minicurriculo" accept="application/pdf"
                                       id="minicurriculo" value="<?= old('minicurriculo') ?>" required>
                                <span class="helper-text" data-error="Informe um arquivo válido."
                                      data-success=""></span>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" name="minicurriculoPath"
                                       placeholder="Minicurrículo" accept="application/pdf" id="minicurriculoPath"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center">
                    <a href="<?= route_to('homePage') ?>" class="btn waves-effect grey lighten-1 mr-3" type="button">
                        Voltar
                    </a>

                    <button class="btn waves-effect waves-light mr-3" type="submit">
                        Criar Conta
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
