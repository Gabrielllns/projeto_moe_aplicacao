<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<input type="hidden" value="cadastroVaga" id="page">

<div class="section">
    <div class="row">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h5 class="text-default">Criar Vaga</h5>

            <span class="text-danger">* Campos obrigatórios</span>
        </div>

        <div class="row">
            <?= $this->include('partials/alertas') ?>

            <form id="formCadastroVaga" method="POST" action="<?= route_to('salvarVaga') ?>" autocomplete="off">
                <div class="row">
                    <div class="input-field col s4">
                        <input id="semestreCurso" type="text" name="semestreCurso" maxlength="20"
                               class="validate" value="<?= old('semestreCurso') ?>" required>
                        <label for="semestreCurso">Semestre do Curso</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>

                    <div class="input-field col s4">
                        <i class="material-icons suffix tooltipped"
                           data-tooltip="A remuneração não pode ser inferior a 600 reais.">info</i>
                        <input id="remuneracao" type="text" name="remuneracao" maxlength="10" class="validate"
                               value="<?= old('remuneracao') ?>" required>
                        <label for="remuneracao">Remuneração</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>

                    <div class="input-field col s4">
                        <select id="qtdeHoras" class="validate" name="qtdeHoras" required>
                            <option value="" disabled selected>Selecione</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                        </select>
                        <label for="qtdeHoras">Qtde de Horas</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="dsResumidaVaga" class="materialize-textarea validate" name="dsResumidaVaga"
                                  data-length="500" required><?= old('dsResumidaVaga') ?></textarea>
                        <label for="dsResumidaVaga">Descrição Resumida da Vaga</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="atividades" class="materialize-textarea validate" name="atividades"
                                  data-length="500" required><?= old('atividades') ?></textarea>
                        <label for="atividades">Atividades</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="habilidades" class="materialize-textarea validate" name="habilidades"
                                  data-length="500" required><?= old('habilidades') ?></textarea>
                        <label for="habilidades">Habilidades</label>
                        <span class="helper-text" data-error="Campo de preenchimento obrigatório"
                              data-success=""></span>
                    </div>
                </div>

                <div class="row text-center">
                    <a href="<?= route_to('listarVagasPage') ?>" class="btn waves-effect grey lighten-1 mr-3"
                       type="button"> Voltar
                    </a>

                    <button class="btn waves-effect waves-light mr-3" type="submit">
                        Cadastrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
