<?= $this->extend('templates/default') ?>

<?= $this->section('nav') ?>
<?= $this->include('partials/nav') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <input type="hidden" value="" id="excluirVaga">
    <input type="hidden" value="listarVagas" id="page">

    <div class="section">
        <div class="row">
            <div class="d-flex justify-content-between align-items-center mb-4">
                <h5 class="text-default">Listar Vagas</h5>

                <a href="<?= route_to('addVagaPage') ?>" class="waves-effect waves-light btn">
                    Criar Vaga
                </a>
            </div>

            <div class="row">
                <?= $this->include('partials/alertas') ?>

                <table class="highlight responsive-table centered">
                    <thead class="grey lighten-2">
                    <tr>
                        <th>Remuneração</th>
                        <th>Semestre do Curso</th>
                        <th>Qtde Horas</th>
                        <th>Descrição</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($data['vagas'])): ?>
                        <?php foreach ($data['vagas'] as $vaga): ?>
                            <tr>
                                <td>R$ <?= $vaga->remuneracao ?></td>
                                <td><?= $vaga->semestreCurso ?></td>
                                <td><?= $vaga->qtdeHoras ?></td>
                                <td><?= $vaga->descricao ?></td>
                                <td>
                                    <a href="<?= route_to('visualizarVagaPage', $vaga->id) ?>">
                                        <i class="material-icons text-info mx-1 icon" title="Visualizar Vaga">search</i>
                                    </a>
                                    <a href="<?= route_to('alterarVagaPage', $vaga->id) ?>">
                                        <i class="material-icons text-primary mx-1 icon" title="Alterar Vaga">edit</i>
                                    </a>
                                    <a class="modal-trigger" href="#modalExclusaoVaga"
                                       data-content="<?= route_to('deletarVaga', $vaga->id) ?>">
                                        <i class="material-icons text-danger mx-1 icon" title="Excluir Vaga">close</i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>

                <?= $data['pager']->links() ?>
            </div>
        </div>
    </div>

    <div id="modalExclusaoVaga" class="modal">
        <div class="modal-content">
            <h6>Deseja realmente excluir a vaga?</h6>
        </div>

        <div class="modal-footer">
            <a id="btnExcluirVaga" class="modal-close waves-effect waves-red btn-flat">Sim</a>
            <a class="modal-close waves-effect waves-green btn-flat">Não</a>
        </div>
    </div>
<?= $this->endSection() ?>