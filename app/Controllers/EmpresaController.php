<?php

namespace App\Controllers;

use App\Business\EmpresaBusiness;

/**
 * Classe de controle de Empresa.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class EmpresaController extends BaseController
{
    /**
     * @var EmpresaBusiness
     */
    private $empresaBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->empresaBusiness)) {
            $this->empresaBusiness = new EmpresaBusiness();
        }
    }

    /**
     * Recupera a empresa por meio do nome informado.
     *
     * @return \CodeIgniter\Database\ResultInterface|\CodeIgniter\HTTP\RedirectResponse|false|string
     * @throws \Exception
     */
    public function pesquisarEmpresaPorNome()
    {
        $vagasEmpresa = $this->empresaBusiness->pesquisarEmpresaPorNome($this->request->getPost('nomeEmpresa'));
        return view('home', compact('vagasEmpresa'));
    }

}
