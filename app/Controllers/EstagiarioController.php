<?php

namespace App\Controllers;

use App\Business\EstagiarioBusiness;

/**
 * Classe de controle de Estagiario.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class EstagiarioController extends BaseController
{
    /**
     * @var EstagiarioBusiness
     */
    private $estagiarioBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->estagiarioBusiness)) {
            $this->estagiarioBusiness = new EstagiarioBusiness();
        }
    }
}
