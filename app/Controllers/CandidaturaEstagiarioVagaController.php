<?php

namespace App\Controllers;

use App\Business\CandidaturaEstagiarioVagaBusiness;

/**
 * Classe de controle de CandidaturaEstagiarioVaga.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class CandidaturaEstagiarioVagaController extends BaseController
{
    /**
     * @var CandidaturaEstagiarioVagaBusiness
     */
    private $candidaturaEstagiarioVagaBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->candidaturaEstagiarioVagaBusiness)) {
            $this->candidaturaEstagiarioVagaBusiness = new CandidaturaEstagiarioVagaBusiness();
        }
    }

    /**
     * Cadastra a candidatura do estagiário autenticado na vaga informada.
     *
     * @param int $idVaga
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function adicionarCandidatura($idVaga)
    {
        $this->candidaturaEstagiarioVagaBusiness->adicionarCandidatura($idVaga);
        return redirect()->route('visualizarVagaPage', [$idVaga]);
    }

    /**
     * Remove a candidatura do estagiário autenticado na vaga informada.
     *
     * @param int $idVaga
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function removerCandidatura($idVaga)
    {
        $this->candidaturaEstagiarioVagaBusiness->removerCandidatura($idVaga);
        return redirect()->route('visualizarVagaPage', [$idVaga]);
    }
}
