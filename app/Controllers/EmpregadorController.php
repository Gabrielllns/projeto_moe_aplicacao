<?php

namespace App\Controllers;

use App\Business\EmpregadorBusiness;

/**
 * Classe de controle de Empregador.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class EmpregadorController extends BaseController
{
    /**
     * @var EmpregadorBusiness
     */
    private $empregadorBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->empregadorBusiness)) {
            $this->empregadorBusiness = new EmpregadorBusiness();
        }
    }
}
