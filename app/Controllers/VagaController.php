<?php

namespace App\Controllers;

use App\Business\VagaBusiness;
use App\Business\CandidaturaEstagiarioVagaBusiness;

/**
 * Classe de controle de Vaga.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class VagaController extends BaseController
{
    /**
     * @var VagaBusiness
     */
    private $vagaBusiness;

    /**
     * @var CandidaturaEstagiarioVagaBusiness
     */
    private $interesseVagaEstagiarioBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->vagaBusiness)) {
            $this->vagaBusiness = new VagaBusiness();
        }
    }

    /**
     * Retorna a página 'cadastroVaga'.
     *
     * @return string
     */
    public function cadastroVaga()
    {
        return view('cadastro-vaga');
    }

    /**
     * Retorna as página 'listarVagas' e as vagas cadastradas.
     *
     * @return string
     */
    public function listarVagas()
    {
        $data = $this->vagaBusiness->findAllPaginate();
        return view('listar-vagas', compact('data'));
    }

    /**
     * Retorna as página 'visualizarVaga' e as vaga por meio do id informado.
     *
     * @param int $id
     * @return string
     */
    public function visualizarVaga($id)
    {
        $vaga = $this->vagaBusiness->find($id);

        $hasCandidatura = false;
        if (session(SESSION_USER)->perfil == PERFIL_ESTAGIARIO) {
            $hasCandidatura = $this->getInteresseVagaEstagiarioBusiness()->hasEstagiarioInteresseVaga($id);
        }

        return view('visualizar-vaga', compact('vaga', 'hasCandidatura'));
    }

    /**
     * Retorna as vagas da empresa por meio do seu id.
     *
     * @param int $id
     * @return string
     */
    public function listarVagasPorEmpresa($id)
    {
        $data = $this->vagaBusiness->findAllVagasPorEmpresaPaginate($id);
        return view('listar-vagas-empresa', compact('data'));
    }

    /**
     * Retorna as página 'informacoesVaga' e as vaga por meio do id informado.
     *
     * @param string $idToken
     * @return string
     */
    public function informacoesVaga($idToken)
    {
        $vaga = $this->vagaBusiness->find(base64_decode($idToken));
        return view('informacao-vaga', compact('vaga'));
    }

    /**
     * Retorna as página 'alterarVaga' e as vaga por meio do id informado.
     *
     * @param int $id
     * @return string
     */
    public function alterarVaga($id)
    {
        $vaga = $this->vagaBusiness->find($id);
        return view('alterar-vaga', compact('vaga'));
    }

    /**
     * Remove a vaga por meio do id informado.
     *
     * @param int $id
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function deletarVaga($id)
    {
        return $this->vagaBusiness->delete($id);
    }

    /**
     * Salva os dados da vaga.
     *
     * @return \CodeIgniter\HTTP\RedirectResponse|string
     * @throws \Exception
     */
    public function salvarVaga()
    {
        return $this->vagaBusiness->salvarVaga($this->request->getPost());
    }

    /**
     * Método de retorno da instância de 'CandidaturaEstagiarioVagaBusiness'.
     *
     * @return CandidaturaEstagiarioVagaBusiness
     */
    private function getInteresseVagaEstagiarioBusiness(): CandidaturaEstagiarioVagaBusiness
    {
        if (is_null($this->interesseVagaEstagiarioBusiness)) {
            $this->interesseVagaEstagiarioBusiness = new CandidaturaEstagiarioVagaBusiness();
        }

        return $this->interesseVagaEstagiarioBusiness;
    }
}
