<?php

namespace App\Controllers;

use App\Business\InteresseEstagiarioEmpresaBusiness;

/**
 * Classe de controle de InteresseEstagiarioEmpresa.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class InteresseEstagiarioEmpresaController extends BaseController
{
    /**
     * @var InteresseEstagiarioEmpresaBusiness
     */
    private $interesseEstagiarioEmpresaBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->interesseEstagiarioEmpresaBusiness)) {
            $this->interesseEstagiarioEmpresaBusiness = new InteresseEstagiarioEmpresaBusiness();
        }
    }

    /**
     * Cadastra o interesse do estagiário autenticado na empresa informada.
     *
     * @param int $idVaga
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function adicionarInteresse($idVaga)
    {
        $this->interesseEstagiarioEmpresaBusiness->adicionarInteresse($idVaga);
        return redirect()->route('homePage');
    }

    /**
     * Remove o interesse do estagiário autenticado na empresa informada.
     *
     * @param int $idVaga
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function removerInteresse($idVaga)
    {
        $this->interesseEstagiarioEmpresaBusiness->removerInteresse($idVaga);
        return redirect()->route('homePage');
    }
}
