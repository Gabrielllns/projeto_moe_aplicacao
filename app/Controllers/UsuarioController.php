<?php

namespace App\Controllers;

use App\Business\UsuarioBusiness;

/**
 * Classe de controle de Usuario.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class UsuarioController extends BaseController
{
    /**
     * @var UsuarioBusiness
     */
    private $usuarioBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->usuarioBusiness)) {
            $this->usuarioBusiness = new UsuarioBusiness();
        }
    }

    /**
     * Retorna a página 'cadastro-usuario'.
     *
     * @return string
     */
    public function cadastroUsuario()
    {
        return view('cadastro-usuario');
    }

    /**
     * Realiza o cadastro do usuário e suas dependências.
     *
     * @return \CodeIgniter\HTTP\RedirectResponse|string
     * @throws \Exception
     */
    public function cadastrarUsuario()
    {
        $data = $this->request->getPost();
        if (intval($data['tpAcesso']) == PERFIL_ESTAGIARIO) {
            $data['minicurriculo'] = $this->request->getFile('minicurriculo');
        }

        return $this->usuarioBusiness->cadastrarUsuario($data);
    }

    /**
     * Confirma o cadastro do usuário conforme o token recebido.
     *
     * @return bool|\CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function confirmarCadastroUsuario()
    {
        return $this->usuarioBusiness->confirmarCadastroUsuario($this->request->getPost());
    }

    /**
     * Valida o cadastro do usuário conforme o token recebido.
     *
     * @param string $token
     * @return bool|\CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function validarCadastroUsuario($token = null)
    {
        if (empty($token) || !is_null($this->usuarioBusiness->getUsuarioAtivoPorId(intval(base64_decode($token))))) {
            return redirect()->route('homePage');
        }

        return redirect()->route('pesquisaSatisfacaoPage', [$token])->with('token', $token);
    }
}
