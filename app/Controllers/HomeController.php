<?php

namespace App\Controllers;

use App\Business\UsuarioBusiness;
use App\Business\VagaBusiness;

/**
 * Classe de controle de Home.
 *
 * @package App\Controllers
 * @author Gabrielllns
 */
class HomeController extends BaseController
{
    /**
     * @var UsuarioBusiness
     */
    private $usuarioBusiness;

    /**
     * @var VagaBusiness
     */
    private $vagaBusiness;

    /**
     * Construtor do controller.
     */
    public function __construct()
    {
        if (is_null($this->usuarioBusiness)) {
            $this->usuarioBusiness = new UsuarioBusiness();
        }
    }

    /**
     * Retorna a página 'home'.
     *
     * @return string
     */
    public function index()
    {
        $ultimasVagas = $this->getVagaBusiness()->findLastVagas();
        return view('home', compact('ultimasVagas'));
    }

    /**
     * Retorna a página 'pesquisaSatisfacao'.
     *
     * @param string $token
     * @return \CodeIgniter\HTTP\RedirectResponse|string
     */
    public function pesquisaSatisfacao($token = null)
    {
        if (empty($token) || !is_null($this->usuarioBusiness->getUsuarioAtivoPorId(intval(base64_decode($token))))) {
            return redirect()->route('homePage');
        }

        return view('pesquisa-satisfacao');
    }

    /**
     * Retorna a página 'login'.
     *
     * @return string
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Finaliza a sessão.
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function logout()
    {
        session()->remove(SESSION_USER);
        return redirect()->route('homePage');
    }

    /**
     * Realiza o login a partir dos dados recebidos.
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function realizarLogin()
    {
        if ($this->usuarioBusiness->realizarLogin($this->request->getPost())) {
            return redirect()->back()->withInput();
        } else {
            return redirect()->route('homePage');
        }
    }

    /**
     * Método de retorno da instância de 'VagaBusiness'.
     *
     * @return VagaBusiness
     */
    private function getVagaBusiness(): VagaBusiness
    {
        if (is_null($this->vagaBusiness)) {
            $this->vagaBusiness = new VagaBusiness();
        }

        return $this->vagaBusiness;
    }
}
