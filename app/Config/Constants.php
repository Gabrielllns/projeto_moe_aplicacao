<?php

/*
 | --------------------------------------------------------------------
 | App Namespace
 | --------------------------------------------------------------------
 |
 | This defines the default Namespace that is used throughout
 | CodeIgniter to refer to the Application directory. Change
 | this constant to change the namespace that all application
 | classes should use.
 |
 | NOTE: changing this will require manually modifying the
 | existing namespaces of App\* namespaced-classes.
 */
defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'ProjetoMOE');

/*
 | --------------------------------------------------------------------------
 | Composer Path
 | --------------------------------------------------------------------------
 |
 | The path that Composer's autoload file is expected to live. By default,
 | the vendor folder is in the Root directory, but you can customize that here.
 */
defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

/*
 |--------------------------------------------------------------------------
 | Timing Constants
 |--------------------------------------------------------------------------
 |
 | Provide simple ways to work with the myriad of PHP functions that
 | require information to be in seconds.
 */
defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR') || define('HOUR', 3600);
defined('DAY') || define('DAY', 86400);
defined('WEEK') || define('WEEK', 604800);
defined('MONTH') || define('MONTH', 2592000);
defined('YEAR') || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

/*
 | --------------------------------------------------------------------------
 | Exit Status Codes
 | --------------------------------------------------------------------------
 |
 | Used to indicate the conditions under which the script is exit()ing.
 | While there is no universal standard for error codes, there are some
 | broad conventions.  Three such conventions are mentioned below, for
 | those who wish to make use of them.  The CodeIgniter defaults were
 | chosen for the least overlap with these conventions, while still
 | leaving room for others to be defined in future versions and user
 | applications.
 |
 | The three main conventions used for determining exit status codes
 | are as follows:
 |
 |    Standard C/C++ Library (stdlibc):
 |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
 |       (This link also contains other GNU-specific conventions)
 |    BSD sysexits.h:
 |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
 |    Bash scripting:
 |       http://tldp.org/LDP/abs/html/exitcodes.html
 |
 */
defined('EXIT_SUCCESS') || define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') || define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') || define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') || define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') || define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') || define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') || define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') || define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') || define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
 |--------------------------------------------------------------------------
 | App Constants
 |--------------------------------------------------------------------------
 */
define('PERFIL_ESTAGIARIO', 1);
define('PERFIL_EMPREGADOR', 2);
define('SESSION_USER', 'authUser');
define('PASTA_UPLOAD_CURRICULOS', 'uploads');
define('TIMEZONE', 'America/Sao_Paulo');

/*
 |--------------------------------------------------------------------------
 | App Table Name
 |--------------------------------------------------------------------------
 */
define('TABLE_EMPRESA', 'empresa');
define('TABLE_EMPREGADOR', 'empregador');
define('TABLE_ESTAGIARIO', 'estagiario');
define('TABLE_INTERESSE_ESTAGIARIO_EMPRESA', 'interesse_estagiario_empresa');
define('TABLE_CANDIDATURA_ESTAGIARIO_VAGA', 'candidatura_estagiario_vaga');
define('TABLE_USUARIO', 'usuario');
define('TABLE_VAGA', 'vaga');

/*
 |--------------------------------------------------------------------------
 | App Messages
 |--------------------------------------------------------------------------
 */
define('ERRO_APLICACAO', 'Erro na aplicação.');
define('CADASTRO_ATIVADO', 'Cadastro ativado.');
define('LOG_EMAILS_ENVIADOS_COM_SUCESSO', 'E-mail(s) enviado(os) com sucesso.');
define('CADASTRO_REALIZADO_SUCESSO', 'Cadastro realizado com sucesso.');
define('ALTERACAO_REALIZADA_SUCESSO', 'Alteração realizada com sucesso.');
define('ERRO_EXECUTAR_ACAO', 'Erro ao realizar a ação.');
define('SEM_PERMISSOES_REALIZAR_ACAO', 'Sem permissões para realizar a ação.');
define('EXCLUSAO_REALIZADA_SUCESSO', 'Exclusão realizada com sucesso.');
define('DADOS_INVALIDOS_PREENCHER_CORRETAMENTE', 'Os dados são inválidos. Favor preencher novamente.');
define('VERIFICAR_EMAIL_CONFIRMAR_CADASTRO', 'Verifique seu e-mail para confirmar seu cadastro.');
define('LINK_INVALIDO_ATIVACAO', 'Link de ativação expirado.');
define('USUARIO_NAO_ENCONTRADO', 'Usuário não encontrado.');
define('FORMATO_ARQUIVO_INVALIDO_INFORME_PDF', 'Formato de arquivo inválido, insira um arquivo PDF.');
define('FAVOR_ADICIONAR_ARQUIVO_ATE_3MB', 'Favor adicionar um arquivo de até 3MB.');
define('ERRO_EMAIL_CADASTRADO', 'E-mail já cadastrado.');

