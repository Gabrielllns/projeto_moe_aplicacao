<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// HOME
$routes->get('/', 'HomeController::index', [
    'as' => 'homePage'
]);

$routes->get('/login', 'HomeController::login', [
    'as' => 'loginPage'
]);

$routes->get('/logout', 'HomeController::logout', [
    'as' => 'logoutPage'
]);

$routes->get('/pesquisa/(:any)', 'HomeController::pesquisaSatisfacao/$1', [
    'as' => 'pesquisaSatisfacaoPage'
]);

$routes->post('/empresa/pesquisa', 'EmpresaController::pesquisarEmpresaPorNome', [
    'as' => 'pesquisarEmpresaPorNome'
]);

$routes->post('/login', 'HomeController::realizarLogin', [
    'as' => 'realizarLogin'
]);

// USUÁRIO
$routes->group('usuario', function ($routes) {
    $routes->get('', function () {
        return redirect()->route('homePage');
    });

    $routes->get('add', 'UsuarioController::cadastroUsuario', [
        'as' => 'addUsuarioPage'
    ]);

    $routes->get('confirmacao/(:any)', 'UsuarioController::validarCadastroUsuario/$1', [
        'as' => 'validarUsuario'
    ]);

    $routes->post('add', 'UsuarioController::cadastrarUsuario', [
        'as' => 'cadastrarUsuario'
    ]);

    $routes->post('confirmacao', 'UsuarioController::confirmarCadastroUsuario', [
        'as' => 'confirmarUsuario'
    ]);
});

// VAGAS
$routes->group('vaga', function ($routes) {
    $routes->get('', function () {
        return redirect()->route('homePage');
    });

    $routes->get('add', 'VagaController::cadastroVaga', [
        'as' => 'addVagaPage',
        'filter' => 'auth-empregador'
    ]);

    $routes->get('list/empresa/(:num)', 'VagaController::listarVagasPorEmpresa/$1', [
        'as' => 'listarVagasPorEmpresa',
        'filter' => 'auth-estagiario'
    ]);

    $routes->get('list', 'VagaController::listarVagas', [
        'as' => 'listarVagasPage',
        'filter' => 'auth-empregador'
    ]);

    $routes->get('alter/(:num)', 'VagaController::alterarVaga/$1', [
        'as' => 'alterarVagaPage',
        'filter' => 'auth-empregador'
    ]);

    $routes->get('delete/(:num)', 'VagaController::deletarVaga/$1', [
        'as' => 'deletarVaga',
        'filter' => 'auth-empregador'
    ]);

    $routes->get('alter/(:num)', 'VagaController::alterarVaga/$1', [
        'as' => 'alterarVagaPage',
        'filter' => 'auth-empregador'
    ]);

    $routes->get('info/(:any)', 'VagaController::informacoesVaga/$1', [
        'as' => 'informacoesVaga'
    ]);

    $routes->get('view/(:num)', 'VagaController::visualizarVaga/$1', [
        'as' => 'visualizarVagaPage',
        'filter' => 'auth'
    ]);

    $routes->post('save', 'VagaController::salvarVaga', [
        'as' => 'salvarVaga',
        'filter' => 'auth-empregador'
    ]);
});

// CANDIDATURA ESTAGIÁRIO VAGA
$routes->group('candidatura-estagiario-vaga', function ($routes) {
    $routes->get('', function () {
        return redirect()->route('homePage');
    });

    $routes->get('add/vaga/(:num)', 'CandidaturaEstagiarioVagaController::adicionarCandidatura/$1', [
        'as' => 'adicionarCandidatura',
        'filter' => 'auth-estagiario'
    ]);

    $routes->get('remover/vaga/(:num)', 'CandidaturaEstagiarioVagaController::removerCandidatura/$1', [
        'as' => 'removerCandidatura',
        'filter' => 'auth-estagiario'
    ]);
});

// INTERESSE ESTAGIÁRIO EMPRESA
$routes->group('candidatura-estagiario-empresa', function ($routes) {
    $routes->get('', function () {
        return redirect()->route('homePage');
    });

    $routes->get('add/vaga/(:num)', 'InteresseEstagiarioEmpresaController::adicionarInteresse/$1', [
        'as' => 'adicionarInteresse',
        'filter' => 'auth-estagiario'
    ]);

    $routes->get('remover/vaga/(:num)', 'InteresseEstagiarioEmpresaController::removerInteresse/$1', [
        'as' => 'removerInteresse',
        'filter' => 'auth-estagiario'
    ]);
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
