<?php

namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Classe de entidade de 'Vaga'.
 *
 * @author Gabrielllns
 * @package App\Entity
 */
class Vaga extends Entity
{
    /**
     * Cria uma nova instância de Vaga.
     *
     * @param array $data
     * @return array
     */
    public static function newInstance(array $data): array
    {
        return [
            'fk_empresa' => 1,
            'semestreCurso' => $data['semestreCurso'],
            'remuneracao' => str_replace(',', '.', str_replace('.', '', $data['remuneracao'])),
            'qtdeHoras' => $data['qtdeHoras'],
            'descricao' => $data['dsResumidaVaga'],
            'atividades' => $data['atividades'],
            'habilidades' => $data['habilidades']
        ];
    }
}