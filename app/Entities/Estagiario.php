<?php

namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Classe de entidade de 'Estagiario'.
 *
 * @author Gabrielllns
 * @package App\Entity
 */
class Estagiario extends Entity
{
    /**
     * Cria uma nova instância de Estagiario.
     *
     * @param array $data
     * @return array
     */
    public static function newInstance(array $data): array
    {
        return [
            'curso' => $data['curso'],
            'fk_usuario' => intval($data['fk_usuario']),
            'anoIngresso' => intval($data['anoIngresso']),
            'minicurriculo' => (!empty($data['minicurriculo'])) ? $data['minicurriculo'] : null,
        ];
    }
}