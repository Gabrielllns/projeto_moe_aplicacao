<?php

namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Classe de entidade de 'Empresa'.
 *
 * @author Gabrielllns
 * @package App\Entity
 */
class Empresa extends Entity
{
    /**
     * Cria uma nova instância de Empresa.
     *
     * @param array $data
     * @return array
     */
    public static function newInstance(array $data): array
    {
        return [
            'nome' => $data['nomeEmpresa'],
            'cep' => $data['cep'],
            'fk_empregador' => $data['fk_empregador'],
            'logradouro' => $data['logradouro'],
            'bairro' => $data['bairro'],
            'complemento' => $data['complemento'],
            'descricao' => $data['dsEmpresa'],
        ];
    }
}