<?php

namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Classe de entidade de 'Empregador'.
 *
 * @author Gabrielllns
 * @package App\Entity
 */
class Empregador extends Entity
{
    /**
     * Cria uma nova instância de Empregador.
     *
     * @param array $data
     * @return array
     */
    public static function newInstance(array $data): array
    {
        return [
            'fk_usuario' => intval($data['fk_usuario'])
        ];
    }
}