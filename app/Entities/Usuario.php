<?php

namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Classe de entidade de 'Usuário'.
 *
 * @author Gabrielllns
 * @package App\Entity
 */
class Usuario extends Entity
{
    /**
     * Cria uma nova instância de Usuario.
     *
     * @param array $data
     * @return array
     */
    public static function newInstance(array $data): array
    {
        return [
            'email' => $data['email'],
            'perfil' => $data['tpAcesso'],
            'nome' => $data['nomeUsuario'],
            'senha' => md5($data['senha']),
            'ativo' => false
        ];
    }
}