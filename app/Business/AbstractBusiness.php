<?php

namespace App\Business;

/**
 * Classe abstrata dos Business.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class AbstractBusiness
{
    /**
     * @var UsuarioBusiness
     */
    protected $usuarioBusiness;

    /**
     * @var EmpregadorBusiness
     */
    protected $empregadorBusiness;

    /**
     * @var EstagiarioBusiness
     */
    protected $estagiarioBusiness;

    /**
     * @var VagaBusiness
     */
    protected $vagaBusiness;

    /**
     * @var EmpresaBusiness
     */
    protected $empresaBusiness;

    /**
     * @var CandidaturaEstagiarioVagaBusiness
     */
    protected $candidaturaEstagiarioVagaBusiness;

    /**
     * @var InteresseEstagiarioEmpresaBusiness
     */
    protected $interesseEstagiarioEmpresaBusiness;

    /**
     * Retorna a instância de uma conexão.
     *
     * @return \CodeIgniter\Database\BaseConnection
     */
    protected function initConexao()
    {
        return \Config\Database::connect();
    }

    /**
     * Método de retorno da instância de 'UsuarioBusiness'.
     *
     * @return UsuarioBusiness
     */
    protected function getUsuarioBusiness(): UsuarioBusiness
    {
        if (is_null($this->usuarioBusiness)) {
            $this->usuarioBusiness = new UsuarioBusiness();
        }

        return $this->usuarioBusiness;
    }

    /**
     * Método de retorno da instância de 'EstagiarioBusiness'.
     *
     * @return EstagiarioBusiness
     */
    protected function getEstagiarioBusiness(): EstagiarioBusiness
    {
        if (is_null($this->estagiarioBusiness)) {
            $this->estagiarioBusiness = new EstagiarioBusiness();
        }

        return $this->estagiarioBusiness;
    }

    /**
     * Método de retorno da instância de 'EmpregadorBusiness'.
     *
     * @return EmpregadorBusiness
     */
    protected function getEmpregadorBusiness(): EmpregadorBusiness
    {
        if (is_null($this->empregadorBusiness)) {
            $this->empregadorBusiness = new EmpregadorBusiness();
        }

        return $this->empregadorBusiness;
    }

    /**
     * Método de retorno da instância de 'VagaBusiness'.
     *
     * @return VagaBusiness
     */
    protected function getVagaBusiness(): VagaBusiness
    {
        if (is_null($this->vagaBusiness)) {
            $this->vagaBusiness = new VagaBusiness();
        }

        return $this->vagaBusiness;
    }

    /**
     * Método de retorno da instância de 'EmpresaBusiness'.
     *
     * @return EmpresaBusiness
     */
    protected function getEmpresaBusiness(): EmpresaBusiness
    {
        if (is_null($this->empresaBusiness)) {
            $this->empresaBusiness = new EmpresaBusiness();
        }

        return $this->empresaBusiness;
    }

    /**
     * Método de retorno da instância de 'CandidaturaEstagiarioVagaBusiness'.
     *
     * @return CandidaturaEstagiarioVagaBusiness
     */
    protected function getCandidaturaEstagiarioVagaBusiness(): CandidaturaEstagiarioVagaBusiness
    {
        if (is_null($this->candidaturaEstagiarioVagaBusiness)) {
            $this->candidaturaEstagiarioVagaBusiness = new CandidaturaEstagiarioVagaBusiness();
        }

        return $this->candidaturaEstagiarioVagaBusiness;
    }

    /**
     * Método de retorno da instância de 'InteresseEstagiarioEmpresaBusiness'.
     *
     * @return InteresseEstagiarioEmpresaBusiness
     */
    protected function getInteresseEstagiarioEmpresaBusiness(): InteresseEstagiarioEmpresaBusiness
    {
        if (is_null($this->interesseEstagiarioEmpresaBusiness)) {
            $this->interesseEstagiarioEmpresaBusiness = new InteresseEstagiarioEmpresaBusiness();
        }

        return $this->interesseEstagiarioEmpresaBusiness;
    }
}
