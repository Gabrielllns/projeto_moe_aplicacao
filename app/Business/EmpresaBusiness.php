<?php

namespace App\Business;

use App\Entities\Empresa;
use App\Models\EmpresaModel;
use App\Models\VagaModel;

/**
 * Classe abstrata de Empresa.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class EmpresaBusiness extends AbstractBusiness
{
    const URL_LINK_GENERATE = '/vaga/info/';

    /**
     * @var EmpresaModel
     */
    private $empresaModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->empresaModel)) {
            $this->empresaModel = new EmpresaModel();
        }
    }

    /**
     * Retorna a empresa cadastrada por meio do id informado.
     *
     * @param int $id
     * @return object|EmpresaModel
     */
    public function find($id)
    {
        return $this->empresaModel->find($id);
    }

    /**
     * Retorna todas as empresas cadastradas.
     *
     * @return array|EmpresaModel[]
     */
    public function findAll()
    {
        return $this->empresaModel->findAll();
    }

    /**
     * Recupera a empresa por meio do nome informado.
     *
     * @param $nomeEmpresa
     * @return Empresa[]
     */
    public function pesquisarEmpresaPorNome($nomeEmpresa)
    {
        $empresas = $this->empresaModel->like('nome', $nomeEmpresa)
            ->join(TABLE_VAGA, 'empresa.id = vaga.fk_empresa')
            ->findAll();

        foreach ($empresas as $empresa) {
            $empresa->totalVagas = $this->getVagaBusiness()->getTotalVagasPorEmpresa($empresa->id);

            $empresa->hasCandidatura = false;
            if (!empty(session(SESSION_USER)) && session(SESSION_USER)->idEstagiario == PERFIL_ESTAGIARIO) {
                $empresa->hasCandidatura = $this->getInteresseEstagiarioEmpresaBusiness()->hasEstagiarioInteresseEmpresa(
                    $empresa->id
                );
            }
        }

        return $empresas;
    }

    /**
     * Realiza o cadastro da empresa.
     *
     * @param array $data
     * @return \CodeIgniter\HTTP\RedirectResponse|object|EmpresaModel
     * @throws \Exception
     */
    public function cadastrarEmpresa(array $data)
    {
        try {
            return $this->empresaModel->insert(Empresa::newInstance($data));
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Realiza o envio do email para os interessados nas vagas da empresa.
     *
     * @param VagaModel|object $vagaModel
     * @throws \Exception
     */
    public function enviarEmailInteressadosVagaEmpresa($vagaModel)
    {
        try {
            $empresa = $this->find($vagaModel->fk_empresa);
            $dataEmails = $this->processarListaEmailEstagiarios($vagaModel->fk_empresa);

            $email = \Config\Services::email();
            $email->setTo($dataEmails);
            $email->setFrom(env('EMAIL_DEFAULT'), "MOE");
            $email->setSubject('[MOE] Alerta de Vaga - ' . $empresa->nome);
            $email->setMessage("
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title></title>
            </head>
            <body>" .
                "<p>Prezado(a),</p><br/>" .
                "<p>A empresa <b>" . $empresa->nome . "</b> adicionou uma nova vaga de estágio.</p>" .
                "<br><p>Descrição da vaga: " . $vagaModel->descricao . "</p>" .
                "<br><p>Para mais informações, clique no link abaixo:</p>" .
                "<br><p><a href=" . base_url() . self::URL_LINK_GENERATE . base64_encode($vagaModel->id) . " target='_blank'>Link da vaga</a></p>" .
                "<br><p>Caso tenha algum problema, favor entrar em contato com a equipe do MOE.</p>" .
                "<br><br><p>Atenciosamente, <p>MOE - Mural de Oportunidades de Estágio</p></p>" .
                "</body>
            </html>"
            );

            $email->send();

            log_message('notice', LOG_EMAILS_ENVIADOS_COM_SUCESSO);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Realiza o envio do email para os interessados nas vagas alteradas da empresa.
     *
     * @param VagaModel|object $vagaModel
     * @throws \Exception
     */
    public function enviarEmailInteressadosAlteracaoVagaEmpresa($vagaModel)
    {
        try {
            $empresa = $this->find($vagaModel->fk_empresa);
            $dataEmails = $this->processarListaEmailEstagiarios($vagaModel->fk_empresa);

            $email = \Config\Services::email();
            $email->setTo($dataEmails);
            $email->setFrom(env('EMAIL_DEFAULT'), "MOE");
            $email->setSubject('[MOE] Alteração de Vaga - ' . $empresa->nome);
            $email->setMessage("
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title></title>
            </head>
            <body>" .
                "<p>Prezado(a),</p><br/>" .
                "<p>A empresa <b>" . $empresa->nome . "</b> alterou informações sobre a vaga de estágio.</p>" .
                "<br><p>Descrição da vaga: " . $vagaModel->descricao . "</p>" .
                "<br><p>Para mais informações, clique no link abaixo:</p>" .
                "<br><p><a href=" . base_url() . self::URL_LINK_GENERATE . base64_encode($vagaModel->id) . " target='_blank'>Link da vaga</a></p>" .
                "<br><p>Caso tenha algum problema, favor entrar em contato com a equipe do MOE.</p>" .
                "<br><br><p>Atenciosamente, <p>MOE - Mural de Oportunidades de Estágio</p></p>" .
                "</body>
            </html>"
            );

            $email->send();

            log_message('notice', LOG_EMAILS_ENVIADOS_COM_SUCESSO);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Formata a lista de e-mails dos estagiários interessados nas vagas da empresa informada.
     *
     * @param int $idEmpresa
     * @return array
     */
    private function processarListaEmailEstagiarios($idEmpresa)
    {
        $dataEmails = [];

        $listaEmails = $this->getEstagiarioBusiness()->getEmailsEstagiariosInteressadosVagasPorEmpresa($idEmpresa);
        foreach ($listaEmails as $listaEmail) {
            array_push($dataEmails, $listaEmail['email']);
        }

        return $dataEmails;
    }
}
