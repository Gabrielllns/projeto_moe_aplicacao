<?php

namespace App\Business;

use App\Models\InteresseEstagiarioEmpresaModel;
use CodeIgniter\I18n\Time;

/**
 * Classe abstrata de InteresseEstagiarioEmpresa.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class InteresseEstagiarioEmpresaBusiness extends AbstractBusiness
{
    /**
     * @var InteresseEstagiarioEmpresaModel
     */
    private $interesseEstagiarioEmpresaModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->interesseEstagiarioEmpresaModel)) {
            $this->interesseEstagiarioEmpresaModel = new InteresseEstagiarioEmpresaModel();
        }
    }

    /**
     * Verifica se o estagiário autenticado tem candidatura para a empresa informada.
     *
     * @param int $idEmpresa
     * @return bool
     */
    public function hasEstagiarioInteresseEmpresa($idEmpresa)
    {
        return $this->interesseEstagiarioEmpresaModel->where('fk_estagiario', session(SESSION_USER)->idEstagiario)
                ->where('dtCancelamento IS NULL')
                ->where('fk_empresa', intval($idEmpresa))
                ->countAllResults() > 0;
    }

    /**
     * Cadastra  o interesse do estagiário autenticado na empresa informada.
     *
     * @param $idVaga
     * @return \CodeIgniter\Database\BaseResult|false|int|object|string
     * @throws \Exception
     */
    public function adicionarInteresse($idVaga)
    {
        try {
            helper('date');

            return $this->interesseEstagiarioEmpresaModel->insert([
                'fk_empresa' => intval($idVaga),
                'fk_estagiario' => intval(session(SESSION_USER)->idEstagiario),
                'dtInscricao' => Time::createFromTimestamp(now(TIMEZONE), TIMEZONE),
            ]);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Remove o interesse do estagiário autenticado na empresa informada.
     *
     * @param int $idVaga
     * @return bool|\CodeIgniter\Database\BaseResult|\CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function removerInteresse($idVaga)
    {
        try {
            helper('date');

            return $this->interesseEstagiarioEmpresaModel->where('fk_empresa', $idVaga)
                ->where('fk_estagiario', intval(session(SESSION_USER)->idEstagiario))
                ->where('dtCancelamento IS NULL')
                ->set([
                    'dtCancelamento' => Time::createFromTimestamp(now(TIMEZONE), TIMEZONE)
                ])
                ->update();
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }
}
