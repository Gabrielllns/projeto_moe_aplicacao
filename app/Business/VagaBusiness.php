<?php

namespace App\Business;

use App\Entities\Vaga;
use App\Models\VagaModel;

/**
 * Classe abstrata de Vaga.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class VagaBusiness extends AbstractBusiness
{
    /**
     * @var VagaModel
     */
    private $vagaModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->vagaModel)) {
            $this->vagaModel = new VagaModel();
        }
    }

    /**
     * Retorna todas as vagas cadastradas utilizando a paginação do materialize.
     *
     * @return array
     */
    public function findAllPaginate()
    {
        return [
            'vagas' => $this->vagaModel->orderBy('id', 'asc')->paginate(10),
            'pager' => $this->vagaModel->pager,
        ];
    }

    /**
     * Retorna todas as vagas vinculadas a uma empresa utilizando a paginação do materialize.
     *
     * @param int $id
     * @return array
     */
    public function findAllVagasPorEmpresaPaginate($id)
    {
        return [
            'empresa' => $this->getEmpresaBusiness()->find($id),
            'vagasEmpresa' => $this->getVagasPorEmpresaEstagiario($id),
            'pager' => $this->vagaModel->pager,
        ];
    }

    /**
     * Recupera a lista de vagas vinculadas ao estagiário conforme o id da empresa informada.
     *
     * @param int $id
     * @return array|null
     */
    public function getVagasPorEmpresaEstagiario($id)
    {
        $vagas = $this->vagaModel->where('vaga.fk_empresa', $id)->paginate(10);

        foreach ($vagas as $vaga) {
            $vaga->hasCandidatura = $this->getCandidaturaEstagiarioVagaBusiness()->hasEstagiarioInteresseVaga($vaga->id);
        }

        return $vagas;
    }

    /**
     * Retorna a vaga cadastrada por meio do id informado.
     *
     * @param int $id
     * @return object|VagaModel
     */
    public function find($id)
    {
        return $this->vagaModel->find($id);
    }

    /**
     * Remove a vaga por meio do id informado.
     *
     * @param int $id
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function delete($id)
    {
        if ($this->vagaModel->delete($id)) {
            return redirect()->route('listarVagasPage')->with('success', EXCLUSAO_REALIZADA_SUCESSO);
        } else {
            log_message('error', ERRO_EXECUTAR_ACAO);
            return redirect()->route('listarVagasPage')->with('error', ERRO_EXECUTAR_ACAO);
        }
    }

    /**
     * Retorna todas as vagas cadastradas.
     *
     * @return array|VagaModel[]
     */
    public function findAll()
    {
        return $this->vagaModel->findAll();
    }

    /**
     * Retorna as últimas 6 vagas cadastradas.
     *
     * @return array|VagaModel[]
     */
    public function findLastVagas()
    {
        $vagas = $this->vagaModel->select('empresa.nome AS nomeEmpresa, empresa.id AS idEmpresa, vaga.id AS idVaga, vaga.descricao AS nomeVaga')
            ->join(TABLE_EMPRESA, 'empresa.id = vaga.fk_empresa')
            ->orderBy('vaga.id', 'desc')
            ->limit(6)
            ->get()
            ->getResultObject();

        foreach ($vagas as $vaga) {
            $vaga->totalVagas = $this->getTotalVagasPorEmpresa($vaga->idEmpresa);

            $vaga->hasCandidatura = false;
            if (!empty(session(SESSION_USER)) && session(SESSION_USER)->perfil == PERFIL_ESTAGIARIO) {
                $vaga->hasCandidatura = $this->getInteresseEstagiarioEmpresaBusiness()->hasEstagiarioInteresseEmpresa(
                    $vaga->idEmpresa
                );
            }
        }

        return $vagas;
    }

    /**
     * Recupera o total de vagas vinculadas ao id da empresa informada.
     *
     * @param int $idEmpresa
     * @return int|string
     */
    public function getTotalVagasPorEmpresa($idEmpresa)
    {
        return $this->vagaModel->selectCount('vaga.id', 'totalVagas')
            ->join(TABLE_EMPRESA, 'empresa.id = vaga.fk_empresa')
            ->where('vaga.fk_empresa', $idEmpresa)
            ->countAll();
    }

    /**
     * Salva os dados da vaga.
     *
     * @param array $data
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function salvarVaga(array $data)
    {
        if (session(SESSION_USER)->perfil != PERFIL_EMPREGADOR) {
            return redirect()->route('listarVagasPage')->with('error', SEM_PERMISSOES_REALIZAR_ACAO);
        }

        $db = $this->initConexao();

        try {
            $db->transBegin();
            $vagaAUX = Vaga::newInstance($data);

            if (!empty($data['idVaga'])) {
                $this->vagaModel->update($data['idVaga'], $vagaAUX);
                $message = ALTERACAO_REALIZADA_SUCESSO;

                $this->getEmpresaBusiness()->enviarEmailInteressadosAlteracaoVagaEmpresa(
                    $this->vagaModel->find($data['idVaga'])
                );
            } else {
                $idVaga = $this->vagaModel->insert($vagaAUX);
                $message = CADASTRO_REALIZADO_SUCESSO;

                $this->getEmpresaBusiness()->enviarEmailInteressadosVagaEmpresa(
                    $this->vagaModel->find($idVaga)
                );
            }

            $db->transCommit();

            return redirect()->route('listarVagasPage')->with('success', $message);
        } catch (\Exception $e) {
            $db->transRollback();
            log_message('error', $e->getMessage());
            return redirect()->route('listarVagasPage')->with('error', ERRO_EXECUTAR_ACAO);
        }
    }
}
