<?php

namespace App\Business;

use App\Models\CandidaturaEstagiarioVagaModel;
use CodeIgniter\I18n\Time;

/**
 * Classe abstrata de CandidaturaEstagiarioVaga.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class CandidaturaEstagiarioVagaBusiness extends AbstractBusiness
{
    /**
     * @var CandidaturaEstagiarioVagaModel
     */
    private $candidaturaEstagiarioVagaModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->candidaturaEstagiarioVagaModel)) {
            $this->candidaturaEstagiarioVagaModel = new CandidaturaEstagiarioVagaModel();
        }
    }

    /**
     * Verifica se o estagiário autenticado tem candidatura para a vaga informada.
     *
     * @param int $idVaga
     * @return bool
     */
    public function hasEstagiarioInteresseVaga($idVaga)
    {
        return $this->candidaturaEstagiarioVagaModel->where('fk_estagiario', session(SESSION_USER)->idEstagiario)
                ->where('dtCancelamento IS NULL')
                ->where('fk_vaga', intval($idVaga))
                ->countAllResults() > 0;
    }

    /**
     * Cadastra a candidatura do estagiário autenticado na vaga informada.
     *
     * @param $idVaga
     * @return \CodeIgniter\Database\BaseResult|false|int|object|string
     * @throws \Exception
     */
    public function adicionarCandidatura($idVaga)
    {
        try {
            helper('date');

            return $this->candidaturaEstagiarioVagaModel->insert([
                'fk_vaga' => intval($idVaga),
                'fk_estagiario' => intval(session(SESSION_USER)->idEstagiario),
                'dtInscricao' => Time::createFromTimestamp(now(TIMEZONE), TIMEZONE),
            ]);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Remove a candidatura do estagiário autenticado na vaga informada.
     *
     * @param int $idVaga
     * @return bool|\CodeIgniter\Database\BaseResult|\CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function removerCandidatura($idVaga)
    {
        try {
            helper('date');

            return $this->candidaturaEstagiarioVagaModel->where('fk_vaga', $idVaga)
                ->where('fk_estagiario', intval(session(SESSION_USER)->idEstagiario))
                ->where('dtCancelamento IS NULL')
                ->set([
                    'dtCancelamento' => Time::createFromTimestamp(now(TIMEZONE), TIMEZONE)
                ])
                ->update();
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }
}
