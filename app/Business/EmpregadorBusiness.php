<?php

namespace App\Business;

use App\Entities\Empregador;
use App\Models\EmpregadorModel;

/**
 * Classe abstrata de Empregador.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class EmpregadorBusiness extends AbstractBusiness
{
    /**
     * @var EmpregadorModel
     */
    private $empregadorModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->empregadorModel)) {
            $this->empregadorModel = new EmpregadorModel();
        }
    }

    /**
     * Busca o empregador por meio do id de usuário informado.
     *
     * @param int $idUsuario
     * @return array|object|null
     */
    public function getEmpregadorPorUsuario($idUsuario)
    {
        return $this->empregadorModel->select('empregador.*')
            ->join(TABLE_USUARIO, 'usuario.id = empregador.fk_usuario')
            ->where('empregador.fk_usuario', $idUsuario)
            ->first();
    }

    /**
     * Realiza o cadastro do empregador.
     *
     * @param array $data
     * @return \CodeIgniter\HTTP\RedirectResponse|object|EmpregadorModel
     * @throws \Exception
     */
    public function cadastrarEmpregador(array $data)
    {
        try {
            $data['fk_empregador'] = $this->empregadorModel->insert(Empregador::newInstance($data));

            $this->getEmpresaBusiness()->cadastrarEmpresa($data);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }
}
