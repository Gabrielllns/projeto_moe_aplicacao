<?php

namespace App\Business;

use App\Entities\Usuario;
use App\Models\UsuarioModel;
use CodeIgniter\HTTP\Files\UploadedFile;

/**
 * Classe abstrata de Usuario.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class UsuarioBusiness extends AbstractBusiness
{
    const URL_LINK_GENERATE = '/usuario/confirmacao/';

    /**
     * @var UsuarioModel
     */
    private $usuarioModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->usuarioModel)) {
            $this->usuarioModel = new UsuarioModel();
        }
    }

    /**
     * Realiza o login a partid dos dados recebidos.
     *
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function realizarLogin(array $data)
    {
        try {
            $usuario = $this->usuarioModel->where('email', $data['email'])
                ->where('senha', md5($data['senha']))
                ->first();

            if (empty($usuario)) {
                session()->setFlashdata('error', DADOS_INVALIDOS_PREENCHER_CORRETAMENTE);
                session()->remove(SESSION_USER);
            } elseif ($usuario->ativo == 0) {
                session()->setFlashdata('error', VERIFICAR_EMAIL_CONFIRMAR_CADASTRO);
                session()->remove(SESSION_USER);
            } else {
                unset($usuario->senha);

                if ($usuario->perfil == PERFIL_ESTAGIARIO) {
                    $usuario->idEstagiario = $this->getEstagiarioBusiness()->getEstagiarioPorUsuario($usuario->id)->id;
                } else {
                    $usuario->idEmpregador = $this->getEmpregadorBusiness()->getEmpregadorPorUsuario($usuario->id)->id;
                }

                session()->set(SESSION_USER, $usuario);
            }

            return empty($usuario) || ($usuario->ativo == 0);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Realiza o cadastro do usuário e suas dependências.
     *
     * @param array $data
     * @return \CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function cadastrarUsuario(array $data)
    {
        $message = $this->validarDados($data);
        if (!is_null($message)) {
            log_message('error', $message);
            return redirect()->back()->withInput()->with('error', $message);
        }

        $db = $this->initConexao();

        try {
            $db->transBegin();

            $data['nomeUsuario'] = (isset($data['nomeCompleto'])) ? $data['nomeCompleto'] : $data['nomeResponsavel'];
            $data['fk_usuario'] = $this->usuarioModel->insert(Usuario::newInstance($data));

            if (intval($data['tpAcesso']) == PERFIL_ESTAGIARIO) {
                $this->getEstagiarioBusiness()->cadastrarEstagiario($data, $data['minicurriculo']);
            } elseif (intval($data['tpAcesso']) == PERFIL_EMPREGADOR) {
                $this->getEmpregadorBusiness()->cadastrarEmpregador($data);
            }

            $this->enviarEmailConfirmacaoCadastro($data);

            $db->transCommit();

            return redirect()->route('loginPage');
        } catch (\Exception $e) {
            $db->transRollback();
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Recupera o usuário por meio do id informado.
     *
     * @param int $id
     * @return array|\CodeIgniter\HTTP\RedirectResponse|object
     */
    public function getUsuarioPorId($id)
    {
        return $this->usuarioModel->find($id);
    }

    /**
     * Recupera o usuário 'ativo' por meio do id informado.
     *
     * @param int $id
     * @return array|\CodeIgniter\HTTP\RedirectResponse|object
     */
    public function getUsuarioAtivoPorId($id)
    {
        return $this->usuarioModel->where('ativo', 1)->find($id);
    }

    /**
     * Confirma o cadastro do usuário conforme o token recebido.
     *
     * @param array $data
     * @return bool|\CodeIgniter\HTTP\RedirectResponse
     * @throws \Exception
     */
    public function confirmarCadastroUsuario(array $data)
    {
        try {
            $usuario = $this->getUsuarioPorId(intval(base64_decode($data['token'])));

            if ($usuario->ativo) {
                return redirect()->route('loginPage')->with('error', LINK_INVALIDO_ATIVACAO);
            } else {
                if (!$this->usuarioModel->update($usuario->id, ['ativo' => true])) {
                    return redirect()->route('loginPage')->with('error', ERRO_APLICACAO);
                }

                return redirect()->route('loginPage')->with('success', CADASTRO_ATIVADO);
            }
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Realiza o envio de confirmação de cadastro dos usuários.
     *
     * @param array $data
     * @throws \Exception
     */
    public function enviarEmailConfirmacaoCadastro(array $data)
    {
        try {
            $email = \Config\Services::email();
            $email->setTo($data['email']);
            $email->setFrom(env('EMAIL_DEFAULT'), "MOE");
            $email->setSubject('[MOE] Confirmação de Cadastro');
            $email->setMessage("
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title></title>
            </head>
            <body>" .
                "<p>Prezado(a),</p><br/>" .
                "<p>Para confirmar o seu cadastro no MOE, por favor, acesse o link abaixo:</p>" .
                "<br><p><a href=" . base_url() . self::URL_LINK_GENERATE . base64_encode($data['fk_usuario']) . " target='_blank'>Link de acesso</a></p>" .
                "<br><p>Caso tenha algum problema, favor entrar em contato com a equipe do MOE.</p>" .
                "<br><br><p>Atenciosamente, <p>MOE - Mural de Oportunidades de Estágio</p></p>" .
                "</body>
            </html>"
            );

            $email->send();

            log_message('notice', LOG_EMAILS_ENVIADOS_COM_SUCESSO);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Valida as informações do minicurriculo e de usuário já cadastrado.
     *
     * @param array $data
     * @return string|null
     */
    private function validarDados(array $data)
    {
        $message = null;

        if (!empty($this->usuarioModel->where('email', $data['email'])->first())) {
            $message = ERRO_EMAIL_CADASTRADO;
        } elseif (intval($data['tpAcesso']) == PERFIL_ESTAGIARIO) {
            $arquivo = $data['minicurriculo'];

            if ($arquivo->getExtension() != 'pdf') {
                $message = FORMATO_ARQUIVO_INVALIDO_INFORME_PDF;
            } elseif ($arquivo->getSize() > 3145728) {
                $message = FAVOR_ADICIONAR_ARQUIVO_ATE_3MB;
            }
        }

        return $message;
    }
}
