<?php

namespace App\Business;

use App\Entities\Estagiario;
use App\Models\EstagiarioModel;
use CodeIgniter\HTTP\Files\UploadedFile;

/**
 * Classe abstrata de Estagiario.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class EstagiarioBusiness extends AbstractBusiness
{
    const URL_UPLOAD_CURRICULOS = WRITEPATH . PASTA_UPLOAD_CURRICULOS;

    /**
     * @var EstagiarioModel
     */
    private $estagiarioModel;

    /**
     * Construtor do business.
     */
    public function __construct()
    {
        if (is_null($this->estagiarioModel)) {
            $this->estagiarioModel = new EstagiarioModel();
        }
    }

    /**
     * Recupera os e-mails dos estagiários interessado em vagas na empresa conforme o id informado.
     *
     * @param int $idEmpresa
     * @return EstagiarioModel[]|array[]
     */
    public function getEmailsEstagiariosInteressadosVagasPorEmpresa($idEmpresa)
    {
        return $this->estagiarioModel->select('usuario.email')
            ->join(TABLE_USUARIO, 'usuario.id = estagiario.fk_usuario')
            ->join(TABLE_INTERESSE_ESTAGIARIO_EMPRESA, 'interesse_estagiario_empresa.fk_estagiario = estagiario.id')
            ->where('interesse_estagiario_empresa.fk_empresa', $idEmpresa)
            ->where('interesse_estagiario_empresa.dtCancelamento IS NULL')
            ->get()
            ->getResultArray();
    }

    /**
     * Busca o estagiário por meio do id de usuário informado.
     *
     * @param int $idUsuario
     * @return array|object|null
     */
    public function getEstagiarioPorUsuario($idUsuario)
    {
        return $this->estagiarioModel->select('estagiario.*')
            ->join(TABLE_USUARIO, 'usuario.id = estagiario.fk_usuario')
            ->where('estagiario.fk_usuario', $idUsuario)
            ->first();
    }

    /**
     * Realiza o cadastro do estagiário.
     *
     * @param array $data
     * @param UploadedFile $arquivo
     * @return \CodeIgniter\Database\BaseResult|false|int|object|string
     * @throws \Exception
     */
    public function cadastrarEstagiario(array $data, UploadedFile $arquivo)
    {
        try {
            $idEstagiario = $this->estagiarioModel->insert(Estagiario::newInstance($data));

            if ($arquivo->isValid() && !$arquivo->hasMoved()) {
                $nomeArquivo = $arquivo->getRandomName();

                if ($arquivo->move(self::URL_UPLOAD_CURRICULOS, $nomeArquivo)) {
                    $this->estagiarioModel->update($idEstagiario, ['minicurriculo' => $nomeArquivo]);
                }
            }

            return $idEstagiario;
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }
}
