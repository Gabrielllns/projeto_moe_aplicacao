<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'InteresseEstagiarioEmpresa'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class InteresseEstagiarioEmpresaModel extends Model
{
    protected $table = TABLE_INTERESSE_ESTAGIARIO_EMPRESA;

    protected $allowedFields = [
        'fk_estagiario',
        'fk_empresa',
        'dtInscricao',
        'dtCancelamento',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = false;
}