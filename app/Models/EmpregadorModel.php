<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'Empregador'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class EmpregadorModel extends Model
{
    protected $table = TABLE_EMPREGADOR;

    protected $allowedFields = [
        'fk_usuario'
    ];

    protected $returnType = 'object';

    protected $useTimestamps = false;
}