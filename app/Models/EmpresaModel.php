<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'Empresa'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class EmpresaModel extends Model
{
    protected $table = TABLE_EMPRESA;

    protected $allowedFields = [
        'nome',
        'cep',
        'logradouro',
        'bairro',
        'fk_empregador',
        'complemento',
        'descricao',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = false;
}