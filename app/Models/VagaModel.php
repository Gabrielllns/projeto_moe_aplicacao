<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'Vaga'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class VagaModel extends Model
{
    protected $table = TABLE_VAGA;

    protected $allowedFields = [
        'fk_empresa',
        'descricao',
        'atividades',
        'habilidades',
        'semestreCurso',
        'qtdeHoras',
        'remuneracao',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = true;
}