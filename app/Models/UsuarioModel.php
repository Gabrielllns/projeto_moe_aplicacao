<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'Usuário'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class UsuarioModel extends Model
{
    protected $table = TABLE_USUARIO;

    protected $allowedFields = [
        'nome',
        'email',
        'senha',
        'perfil',
        'ativo',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = true;
}