<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'CandidaturaEstagiarioVaga'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class CandidaturaEstagiarioVagaModel extends Model
{
    protected $table = TABLE_CANDIDATURA_ESTAGIARIO_VAGA;

    protected $allowedFields = [
        'fk_vaga',
        'fk_estagiario',
        'dtInscricao',
        'dtCancelamento',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = false;
}