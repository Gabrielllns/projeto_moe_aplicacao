<?php

namespace App\Models;

use CodeIgniter\Model;

/**
 * Classe de modelo de 'Estagiario'.
 *
 * @author Gabrielllns
 * @package App\Models
 */
class EstagiarioModel extends Model
{
    protected $table = TABLE_ESTAGIARIO;

    protected $allowedFields = [
        'curso',
        'fk_usuario',
        'anoIngresso',
        'minicurriculo',
    ];

    protected $returnType = 'object';

    protected $useTimestamps = false;
}